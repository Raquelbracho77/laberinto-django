#+TITLE: Guia django 1.2
#+PROPERTY: header-args:shell :results output

* Aplicaciones creadas
 - leyenda
 - lab_icaro
 - lab_corredor

* Construcción de urls
#+begin_src dot :file urls-conexiones.png
digraph urls {
leyenda [label="1 - leyenda/urls.py"];
lab_icaro [label="2 - lab_icaro/urls.py"];
lab_corredor [label ="3 - lab_corredor/urls.py"];
Base [label="Base - urls.py"]
leyenda -> Base;
lab_icaro -> Base;
lab_corredor -> Base;
}
#+end_src

#+RESULTS:
[[file:urls-conexiones.png]]

También resulta útil construir una tabla de diseño de las url, 
a continuación, reverse("name basic:name app")

- app_name :: Nombre de la aplicación
- basic urls :: Es la url que se define en el archivo *principal* de *urls.py*
- name basic :: Es es valor *name* del argumento _opcional_ de path()
- view :: Nombre de la clase en el archivo *view.py* en cada aplicación
- url pattern :: Es el primer parametro en /string/ de *path()* en el archivo *urls.py* de cada aplicación
- name - app :: Nombre en string *app_name* en el archivo *url.py* de cada app
- template :: Es la forma o estructura de la url en el navegador
- reverse check :: Producto final para acceder a la url de la app

| app_name     | basic urls   | name basic | view               | url pattern  | name - app | template                   | reverse check                |
|--------------+--------------+------------+--------------------+--------------+------------+----------------------------+------------------------------|
| leyenda      | leyenda      | leyenda    | IndiceLeyendasView | indice       | indice     | leyenda/index.dj.html      | reverse("leyenda:indice")    |
| lab_icaro    | lab_icaro    | icaro      | LaberintoIcaro     | lab_icaro    | icaro      | lab_icaro/index.dj.html    |                              |
| lab_corredor | lab_corredor | corredor   | LaberintoCorredor  | lab_corredor | corredor   | lab_corredor/index.dj.html | reverse("corredor:corredor") |
|              |              |            |                    |              |            |                            |                              |
|              |              |            |                    |              |            |                            |                              |
|              |              |            |                    |              |            |                            |                              |
|              |              |            |                    |              |            |                            |                              |
|              |              |            |                    |              |            |                            |                              |
|              |              |            |                    |              |            |                            |                              |


El parámetro *name* de cada path() en urls.py, sive para tener 
una referencia del *urlpatterns* registrado.

https://docs.djangoproject.com/en/3.2/ref/urls/

Path se conforma:

#+begin_example
path(route, view, kwargs=None, name=None)
#+end_example

- route :: Es es string que define la ruta
- view :: Es la función que entrega la vista(view)
- kwargs :: Es el diccionario de opciones para la vista(view)
- name :: Es el nombre que le asigno a *route*

** Reverse 
Es una función que nos permite obtener la *url* dada el nombre 
o *name* asignado al url.

 - si:

 - app_name :: leyenda
 - name :: indice

#+begin_src python
from django.urls import reverse
url = reverse("leyenda:indice")
print(url)
#+end_src


** Resolver

*** View
La clase vista-view tiene que convertirse en una funcion vista, ejecutando el metodo de la clase class_view()
para poder enrutarlo y mostralro en el navegador.

#+begin_src python
from django.urls import resolve # importar
url_obj = resolve("/leyenda/indice") #Entrega el objeto de python segun la ruta '''
type(url_obj) # Muestra el tipo de dato que es'''
url_obj.__dict__.keys() #Verifica el contenido de las llaves junto con el diccionario'''
url_obj #'verifica el contenido de obj'''
url_obj.func #Muestra la funcion o clase de la app en el archivo view.py'''
#+end_src

** Enlaces en templates

Usando
