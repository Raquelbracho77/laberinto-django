from django.apps import AppConfig


class GemelosConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gemelos'
