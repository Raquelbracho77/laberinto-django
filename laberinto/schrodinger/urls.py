from django.conf.urls import url
from django.urls import path
from .views import Schrodinger

app_name = "paradoja_sch"

urlpatterns = [
    path("sch", Schrodinger.as_view(), name="paradoja_sch")
]