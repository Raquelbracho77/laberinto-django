"""laberinto URL Configuration GENERAL

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
"""URLS GENERAL"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path("leyenda/",
         include ("leyenda.urls", namespace="leyenda"),
         name="leyenda"),
    # view de laberinto de icaro
    path("lab_icaro/",
         include("lab_icaro.urls", namespace="lab_icaro"),
         name="icaro"),
    # view del corredor del laberinto
    path("lab_corredor/",
         include("lab_corredor.urls", namespace="lab_corredor"),
         name="corredor"),
    #view de pacman
    path("pac_man/",
        include("pac_man.urls", namespace="pac_man"),
        name="pacman"),
    path("schrodinger/",
        include("schrodinger.urls", namespace="paradoja_sch"),
        name="sch")
] + static(
    settings.MEDIA_URL,
    document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += static(
        settings.STATIC_URL,
        document_root=settings.STATIC_ROOT)