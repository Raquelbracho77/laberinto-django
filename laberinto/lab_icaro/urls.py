from django.conf.urls import url
from django.urls import path
from .views import LaberintoIcaro

app_name = "icaro"


urlpatterns = [
    path("lab_icaro", LaberintoIcaro.as_view(), name="icaro")
]
