from django.apps import AppConfig


class LabIcaroConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lab_icaro'
