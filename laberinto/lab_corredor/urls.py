from django.conf.urls import url
from django.urls import path
from .views import LaberintoCorredor

app_name="corredor"

urlpatterns = [
    path("lab_corredor", LaberintoCorredor.as_view(), name="corredor")
]
