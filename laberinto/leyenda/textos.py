diccionario = {
	"icaro":{
		"titulo":"Laberinto de Ícaro",
		"resumen":
			'''
			Ícaro estaba retenido junto a su padre, Dédalo, en la isla de Creta por el rey de la isla, llamado Minos.
			''',
			"imagen":"icaro.jpg",
		"texto":'''
				<p>
				El Laberinto de Creta es, en la mitología griega, el laberinto construido por Dédalo para esconder al Minotauro. 
				En la actualidad, se piensa que la leyenda del laberinto tiene su base en el palacio de Cnosos. 
				Una construcción tan sofisticada y de alta tecnología como dicho palacio, repleto de múltiples habitaciones 
				y con todas las mejoras conocidas por la tecnología de entonces (incluyendo un sistema de alcantarillado) 
				que debió haber parecido a los aqueos algo intrincado. Apoya esta tesis el hecho de que en el palacio de 
				Cnosos se han encontrado dibujos de hachas de doble filo por doquier, que en griego se llaman labrys, y que 
				habrían dado nombre a la construcción.
				</p>
				'''
	},
	"corredor":{
		"titulo":"El corredor del laberinto",
		"resumen":"Thomas es un adolescente cuya memoria fue borrada y que ha sido encerrado junto a otros chicos",
		"imagen":"corredor.jpg",
		"texto":'''
		<p>
		El Laberinto de Creta es, en la mitología griega, el laberinto construido por Dédalo para esconder al Minotauro. 
		En la actualidad, se piensa que la leyenda del laberinto tiene su base en el palacio de Cnosos. Una construcción tan sofisticada y de alta tecnología como dicho palacio, repleto de múltiples habitaciones y con todas las mejoras conocidas por la tecnología de entonces (incluyendo un sistema de alcantarillado) que debió haber parecido a los aqueos algo intrincado. Apoya esta tesis el hecho de que en el palacio de Cnosos se han encontrado dibujos de hachas de doble filo por doquier, que en griego se llaman labrys, y que habrían dado nombre a la construcción.
		</p>
		'''
	},
	"pacman":{
		"titulo":"Pac-man",
		"resumen":"cuatro fantasmas o monstruos,recorren el laberinto para intentar capturar a Pac-Man",
		"imagen":"pacman.png",
		"texto":'''
		<p>Los <b>laberintos</b> son los niveles de <i><a href="/es/wiki/Pac-Man_(videojuego)" title="Pac-Man (videojuego)">Pac-Man</a></i>. En el centro del laberinto siempre hay una <a href="/es/wiki/Casa" title="Casa">casa</a> por donde los <a href="/es/wiki/Fantasmas" title="Fantasmas">fantasmas</a> se generan y regeneran.
		</p><p>Todos los niveles, tienen el mismo laberinto con el mismo color y forma. En el nivel 256 no se puede pasar debido al <a href="/es/wiki/Glitch_del_laberinto_256" title="Glitch del laberinto 256">glitch del laberinto 256</a>, donde la pantalla se divide, haciendo que este sea el nivel final.
		</p>
		'''
	}
}