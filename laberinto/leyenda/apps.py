from django.apps import AppConfig


class LeyendaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'leyenda'
