from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic import ListView, DetailView

from .textos import diccionario
from pprint import pprint
from .models import Leyenda

# Create your views here.

class IndiceLeyendasView(ListView):
    model = Leyenda
    template_name = "leyenda/index.dj.html"
    context_object_name = "leyendas"

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     print(context)
    #     context.update({"leyendas":diccionario})
    #     return context


class LeyendaView(DetailView):
    model = Leyenda
    template_name = "leyenda/texto.dj.html"
    context_object_name = "leyenda"
    slug_field = 'slug'
    slug_url_kwarg = 'slug'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        nombre = context.get('nombre')
        leyenda = diccionario.get(nombre,{})
        if not leyenda:
            leyenda = {"texto":f"No existe leyenda {nombre}"}
        pprint(leyenda)
        context.update(leyenda)
        return context


