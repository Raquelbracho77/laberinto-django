from django.db import models
from django_extensions.db.fields import AutoSlugField
from django.urls import reverse

# Create your models here.
class Leyenda(models.Model):
	titulo = models.CharField(max_length=124)
	fecha = models.DateTimeField(
		auto_now=True)
	slug=AutoSlugField(populate_from="titulo")
	resumen = models.TextField()
	texto = models.TextField()
	imagen = models.ImageField(upload_to="imagenes")
	

	def get_absolute_url(self):
		return reverse("leyenda:nombre",
						kwargs={"nombre":self.slug})

	def __str__(self):
		return f"Leyenda({self.titulo})"

	def __repr__(self):
		return f"Leyenda({self.titulo}, {self.fecha})"

	class Meta:
		app_label = "leyenda"
		verbose_name = "Leyenda"
		verbose_name_plural = "Leyendas"
		ordering = ("-fecha", "titulo")